package hello;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JoelController {


    @GetMapping("/Controlla")
    public String ContollerGet() {
       return "Hello get Method";
    }
    @PostMapping("/Controlla")
    public String ContollerPost() {
       return "Hello post Method";
    }
	

}
