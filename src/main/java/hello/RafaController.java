package hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RafaController {

    @GetMapping("/rafa")
    public String rafaEndpoint(){

        return "Hola Rafa GET";
    }

    @PostMapping("/rafa")
    public  String rafaEndpointPost(){

        return "Hola Rafa POST";
    }


}
